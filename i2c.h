/**
 * @file      : i2c.h
 * @brief     : Linux平台I2C驱动代码头文件
 * @author    : huenrong (huenrong1028@gmail.com)
 * @date      : 2021-04-04 11:36:03
 *
 * @copyright : Copyright (c) 2021 胡恩荣
 *
 */

#ifndef __I2C_H
#define __I2C_H

#include <stdint.h>

/**
 * @brief  打开I2C设备
 * @param  i2c_dev_name: 输入参数, I2C设备名(如: /dev/i2c-0)
 * @return 成功: 0
 *         失败: -1
 */
int i2c_open(const char *i2c_dev_name);

/**
 * @brief  关闭I2C设备
 * @param  i2c_dev_name: 输入参数, I2C设备名(如: /dev/i2c-0)
 * @return 成功: 0
 *         失败: -1
 */
int i2c_close(const char *i2c_dev_name);

/**
 * @brief  向无寄存器地址的I2C从设备发送数据/命令
 * @param  i2c_dev_name : 输入参数, I2C设备名(如: /dev/i2c-0)
 * @param  slave_addr   : 输入参数, I2C从设备地址
 * @param  send_data    : 输入参数, 待发送数据/命令
 * @param  send_data_len: 输入参数, 待发送数据/命令长度
 * @return 成功: 0
 *         失败: -1
 */
int i2c_write_data(const char *i2c_dev_name, const uint16_t slave_addr,
                   const uint8_t *send_data, const uint32_t send_data_len);

/**
 * @brief  从无寄存器地址的I2C从设备读取数据
 * @param  recv_data    : 输出参数, 读取到的数据
 * @param  i2c_dev_name : 输入参数, I2C设备名(如: /dev/i2c-0)
 * @param  slave_addr   : 输入参数, I2C从设备地址
 * @param  recv_data_len: 输入参数, 待读取数据长度
 * @return 成功: 0
 *         失败: -1
 */
int i2c_read_data(uint8_t *recv_data, const char *i2c_dev_name,
                  const uint16_t slave_addr, const uint32_t recv_data_len);

/**
 * @brief  向有寄存器的I2C从设备发送数据/命令(即写I2C从设备寄存器)
 * @param  i2c_dev_name  : 输入参数, I2C设备名(如: /dev/i2c-0)
 * @param  slave_addr    : 输入参数, I2C从设备地址
 * @param  reg_addr      : 输入参数, I2C从设备寄存器地址
 * @param  write_data    : 输入参数, 待发送数据/命令
 * @param  write_data_len: 输入参数, 待发送数据/命令长度
 * @return 成功: 0
 *         失败: -1
 */
int i2c_write_data_sub(const char *i2c_dev_name,
                       const uint16_t slave_addr, const uint8_t reg_addr,
                       const uint8_t *write_data, const uint32_t write_data_len);

/**
 * @brief  从有寄存器地址的I2C从设备读取数据(即读I2C从设备寄存器)
 * @param  recv_data    : 输出参数, 读取到的数据
 * @param  i2c_dev_name : 输入参数, I2C设备名(如: /dev/i2c-0)
 * @param  slave_addr   : 输入参数, I2C从设备地址
 * @param  reg_addr     : 输入参数, I2C从设备寄存器地址
 * @param  recv_data_len: 输入参数, 指定读取长度
 * @return 成功: 0
 *         失败: -1
 */
int i2c_read_data_sub(uint8_t *recv_data,
                      const char *i2c_dev_name, const uint16_t slave_addr,
                      const uint8_t reg_addr, const uint32_t recv_data_len);

#endif // __I2C_H
